import { html, css, LitElement } from 'lit-element';

export class MyWishlist extends LitElement {

  static get properties() {
    return {
      myArray: {type: Array},
      data : {type: String},
      activo: {type: Boolean},

    };
  }

  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--my-wishlist-text-color, #000);
      }
    `;
  }

  constructor() {
    super();
    this.myArray = []; 
    this.activo = false;

  }

handleKeyPress(event) {
    if (event.keyCode == 13) {
      //alert('You pressed enter');
      this.elements = this.shadowRoot.querySelector("#data").value;
      this.myArray.push(this.elements);
      //console.log(this.myArray);
      this.elements = "";
      }
  }

  handleInput(e) {
    this.data = e.target.value;
  }


  render() {
    return html`
      <h1>My wishlist</h1>

      <fieldset>
      <legend>New wish</legend>
        <input type="text" id="data" @input=${this.handleInput} @keypress=${this.handleKeyPress}/>      
      </fieldset>
      <br>
      <ul>
        ${this.myArray.map(i => html`<li><input type="checkbox" id="check" ?checked="${this.activo}" @change="${this.doChange}">${i}</li>`)}      
      </ul>
      <br>
    `;
  }

doChange(e){
    this.activo = e.target.checked;
  console.log('Cambiar activo a: '+this.activo);
  if(this.activo == true){
    //alert('se va a eliminar');
    this.myArray.shift();
    console.log(this.myArray);
  }
}      
}
